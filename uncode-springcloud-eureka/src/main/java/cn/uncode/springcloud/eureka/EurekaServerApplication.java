package cn.uncode.springcloud.eureka;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

import cn.uncode.springcloud.starter.boot.app.AppInfo;
import cn.uncode.springcloud.starter.boot.app.UncodeApplication;

/**
 * @author juny
 */
@EnableEurekaServer
@SpringBootApplication
public class EurekaServerApplication {

    public static void main(String[] args) {
    	UncodeApplication.run(AppInfo.APPLICATION_EUREKA, EurekaServerApplication.class,  args);
    }
}
